#!/bin/bash

# pulseoxdl - pulse oximetry downloader (Contec CMS50E, USB HID)
# Copyright © 2021-2022, 2024 Donatas Klimašauskas
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

# Main.

# Connects two CLI programs' stdins to stdouts and vice versa.
readonly CRISSCROSSER=socat
readonly DATE_TIME_SYNC_CMD=^83

if [ ! $(whereis -b $CRISSCROSSER | awk '{ print $2 }') ]; then
    echo "$0: program '$CRISSCROSSER' is required"

    exit 1
fi

readonly DIRROOT=data/test
readonly DIRTRAN=$DIRROOT/transmission
readonly DIRMANU=$DIRROOT/manufacturer
readonly EXPECTED=$DIRROOT/expected
readonly DIRTMP=$(mktemp -d --tmpdir pulseoxdl-test-XXXX)
readonly TMP=$DIRTMP/tmp.data
readonly SORTED=$TMP.sorted
readonly MOVE_NAME=move
readonly LIVE_NAME=live
readonly ECHO_NAME=echo
readonly LIVE_TRAN=$DIRTRAN/$LIVE_NAME
readonly MOVE_SAVED=20210515120642
readonly LIVE_SAVED=20210430112839
readonly DIRMOVE=$DIRMANU/$MOVE_NAME
readonly MOVECSV=$DIRMOVE/csv

readonly ANALYZER=analyze.awk
readonly ANALYZER_EXPECTED=$DIRROOT/$ANALYZER

do_test ()
{
    echo $1

    $CRISSCROSSER \
	EXEC:"./pulseoxdl /dev/hidrawN $1" \
	EXEC:"./simulator $2" \
	|& tee -a $TMP

    test -z "$3" && return

    local -r ACTION=$DIRMANU/$1/

    diff -q {${ACTION},$3.}csv
    diff {${ACTION},$3.}SpO2
}

do_test $MOVE_NAME "$DIRTRAN/spo2 $DIRTRAN/pr" $MOVE_SAVED
do_test $LIVE_NAME $LIVE_TRAN $LIVE_SAVED
do_test $ECHO_NAME $LIVE_TRAN

# Filter out the date and time synchronization.
grep -v $DATE_TIME_SYNC_CMD $TMP | sort -u >$SORTED

# Include expected echo action output and test for equality.
sort $EXPECTED $DIRMANU/$LIVE_NAME/csv >$TMP
diff -q $TMP $SORTED

NOPLOT=1 ./$ANALYZER $MOVECSV | tee $TMP
diff -q $ANALYZER_EXPECTED $TMP

# Utilities.

readonly DIRUTILS=utils
readonly CSVTOSPO2=$DIRUTILS/csv-to-spo2.pl
readonly SPO2TOCSV=$DIRUTILS/spo2-to-csv.pl
readonly PROGJAAM=$DIRUTILS/join-auto-as-manual.pl
readonly PREPARED=$DIRTMP/prepared.csv

print_utility ()
{
    test -n "$1" && echo -n "$1: " || echo 'OK'
}

test_utility ()
{
    print_utility $1

    $1 <$DIRMOVE/$2 >$TMP
    diff -q $DIRMOVE/$3 $TMP

    print_utility
}

test_utility $CSVTOSPO2 csv SpO2
test_utility $SPO2TOCSV SpO2 csv

# Test "Manual" mode record emulation: "Auto" mode records downloaded
# to a directory and the utility joining program ran to produce a
# single record with the gaps filled as by the "Manual" mode.

print_utility $PROGJAAM

# Prepare the CSV to compare the joined and converted SpO2 to. Set the
# datums to maximums of what the "Manual" mode would set at after an
# hour, for the duration of 5 minutes, and the same again, leaving
# real datums until the end. (First is header, then 1 line 1 s.)
awk '{ if (NR > 3601 && NR < 3902 || NR > 7501 && NR < 7802) { print $1, $2,
"127, 255\r"; next } print }' <$MOVECSV >$PREPARED

# Prepare 3 CSV files as downloaded from the "Auto" mode.
(
    set_finish ()
    {
	let finish=start+window
    }

    start=2 # The first measurement line of the expected CSV.
    window=3599 # The first measurement line has the datums.
    gap=301 # The 1 is due to inverse of the window length.
    set_finish # The last line of a CSV file.
    tmp=tmp.csv

    for i in 1 2 3
    do
	sed -n "1p;$start,${finish}p" $MOVECSV >$tmp
	mv -i $tmp $DIRTMP/$(sed -n '2s/[-T:, ]//gp' $tmp | cut -c -14).csv
	let start=finish+gap
	set_finish
    done
)

# Convert the 3 CSVs to SpO2s.
(
    for csv in $DIRTMP/*.csv
    do
	$CSVTOSPO2 <$csv >${csv/csv/SpO2}
    done
)

# Join the 3 SpO2s: first leave, then remove the used SpO2, CSV files.
(
    join=$(dirname $(realpath $0))/$PROGJAAM
    cd $DIRTMP

    run_join ()
    {
	local count=$(test $1 && echo 0 || echo 6)

	eval ${1:+JAAM_REMOVE_USED=1} $join
	test $(ls *[0-9].{SpO2,csv} 2>/dev/null | wc -l) -eq $count
    }

    run_join
    rm *joined*
    run_join 'remove'
)

# Convert the joined SpO2 to the joined CSV.
(
    spo2=$DIRTMP/$MOVE_SAVED.joined.SpO2
    $SPO2TOCSV <$spo2 >${spo2/SpO2/csv}
)

# The prepared CSV has to match the joined SpO2 converted to CSV.
diff -q $PREPARED $DIRTMP/*.joined.csv

print_utility

# Cleanup.

rm $DIRTMP/*
rmdir $DIRTMP

echo 'pass'

exit 0
