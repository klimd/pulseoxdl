/* pulseoxdl - pulse oximetry downloader (Contec CMS50E, USB HID)
 * Copyright © 2021-2022 Donatas Klimašauskas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>

#include "exchange.h"

enum tests {
	TEST_MOVE,
	TEST_LIVE,
};

static void
respond_data(char *argv[], unsigned char move)
{
	move = move == TEST_MOVE ? 1 : 0;

	if (move)
		read_data(stdin, requestdata);

	FILE *fp;

	open_file(&fp, move ? argv[in[MEASUREMENT_INDEX]] : argv[1], "rb");
	while (1) {
		if (fread(in, 1, LEN_HID_REPORT, fp) != LEN_HID_REPORT)
			if (feof(fp))
				break;
		exit_on_read_error(fp);
		if (fwrite(in, 1, LEN_HID_REPORT, stdout) != LEN_HID_REPORT)
			exit_error("writing to stdout failed");
	}
	close_file(fp);
}

static void
do_exchange(const struct exchange exchange)
{
#ifdef DEBUG
	debug_exchange(exchange);
#endif
	read_data(stdin, exchange.write);
	write_data(stdout, exchange.read);
}

int
main(int argc, char *argv[])
{
	set_program_name(argv[0]);

	if (argc < 2 || argc > 3)
		exit_error("SpO2 and PR or live transmission required");

	unsigned int i;

	unbuf_stdout();
	for (i = 0; i < LEN_EXCHANGES; i++)
		do_exchange(exchanges[i]);

	if (argc == 3) { /* Move. */
		do_exchange(storedauto);
		do_exchange(record);

		/* SpO2 expected first, but could be PR or vice versa. */
		respond_data(argv, TEST_MOVE);
		respond_data(argv, TEST_MOVE);

		do_exchange(delete);
	} else if (argc == 2) { /* Live. */
		read_data(stdin, requestlivedata[0]);
		read_data(stdin, requestlivedata[1]);

		respond_data(argv, TEST_LIVE);

		do_exchange(endlive);
	}

	exit(EXIT_SUCCESS);
}
